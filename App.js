import * as React from 'react';
import { Platform, StyleSheet, Text, View } from 'react-native';
import Home from './screens/Home';
import 'react-native-gesture-handler';
import { NavigationContainer } from '@react-navigation/native';


import NavigationStack from './navigation/NavigationStack';

export default function App() {
  return (
    
      <NavigationStack/>
  
  
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',

  }
});
