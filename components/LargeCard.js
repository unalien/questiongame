import React from "react";
import { View, StyleSheet } from "react-native";

const LargeCard = ({ children, customStyle }) => {
    return (
      <View style={{ flex: 1, backgroundColor: '#8E5CC3' }}>
        <View
          style={[styles.container, customStyle]}
        >
          {children}
        </View>
      </View>
    );
  };

  const styles = StyleSheet.create({
    container:{
      flex: 1,
      borderBottomLeftRadius: 0,
      borderBottomRightRadius: 0,
      paddingTop: 20,
      borderRadius: 20,
      marginTop: 20,
      backgroundColor: 'white',
        
    }
  })

export default LargeCard;  