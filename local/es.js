export default {
    home:{
        firstButton: 'Ir a las preguntas',
        secondButton: 'Empezá a jugar!'
    },
    gameScreen: {
        finishTheGame: 'Terminaste el juego!',
        results: 'Resultado',
        correctAnswers: 'Correctas',
        wrongAnswers: 'Incorrectas',
        restartButton: 'Reintentar'
    }
}