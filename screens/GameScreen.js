import React, { Component } from 'react'
import { Text, View, TouchableOpacity, StyleSheet, Animated} from 'react-native'
import Card from '../components/Card';
import LargeCard from '../components/LargeCard';
import es from '../local/es';

const TOTAL_QUESTIONS = 10

export class GameScreen extends Component {
    constructor(props){
        super(props);
        this.state={
            animatedValue:new Animated.Value(0),
            counter: 0,
            correctAnswers:0,
            questionsGame:[]
        }
        
    }
    
    componentDidMount(){
       this.randomQuestion()
    }

    randomQuestion(){
        const { questions } = this.props.route.params
        const qIndex = [];
        while (qIndex.length < TOTAL_QUESTIONS) {
          const randomNumber = Math.floor(Math.random() * questions.length + 1);
          if (qIndex.indexOf(randomNumber) === -1) {
            qIndex.push(randomNumber);
          }
        }
        console.log(qIndex)
        const questionsGame = questions.filter((question) => {
          return qIndex.indexOf(question.number) === -1 ? false : true;
        });
        this.setState({
            questionsGame
        })
    }
   
    saveAnswers(answer){

        const {animatedValue,counter,correctAnswers} = this.state
        setTimeout(()=>{
            Animated.spring(animatedValue,{
                toValue:0,
                tension:20,
                useNativeDriver:true
            }).start(()=>{
                if(answer.isCorrect){
                    let tmpCorrectAnswers = correctAnswers + 1
                    this.setState({correctAnswers:tmpCorrectAnswers})
                }
                this.setState({counter: counter +1} )
            })
        },100)
     
        
    }
    cleanData(){
        this.setState({correctAnswers: 0, counter: 0})
        this.randomQuestion()
    }

    showResult = () => {

        const {animatedValue,counter,questionsGame} = this.state

        setTimeout(()=>{
            Animated.spring(animatedValue,{
                toValue:1,
                tension:20,
                useNativeDriver:true
            }).start()
        },100)

        const translateY = animatedValue.interpolate({
          inputRange: [0, 1],
          outputRange: [500, 1],
        });
        return (
          <Animated.View
            style={[
              { transform: [{ translateY }] },
            ]}
          >
           {questionsGame[counter].answers.map((answer) => <TouchableOpacity onPress = {()=>this.saveAnswers(answer)}><View style = {styles.bottomStyle}><Text style = {styles.answerStyle}>{answer.answer}</Text></View></TouchableOpacity>)}

          </Animated.View>
        );
      };

    render() {

        const {counter,correctAnswers,questionsGame} = this.state
        
        if(questionsGame.length > 0){
            if (counter < 10){
                return (
                    <View style={styles.container}>
                      
                        <View style = {styles.questionContainerStyle}>
                            <Text style = {styles.questionStyle}>{questionsGame[counter].question}</Text>
                        </View> 
                         
                        <LargeCard>
                            {this.showResult()}

                        </LargeCard>
                                  
                    </View>
                )
            }else{
                return(
                    <View style = {styles.scoreContainer}>
                        <Text style = {styles.finishTextStyle}>
                            {es.gameScreen.finishTheGame}
                        </Text>
                        
                        <Card 
                            containerStyle = {{
                                height: '40%', 
                                justifyContent: 'center', 
                                alignContent: 'center'
                            }}
                        
                        >
                            <Text style = {styles.scoreTextStyle}>{es.gameScreen.results}</Text>
                            <View style = {{flexDirection: 'row', margin: 0, padding: 0}}>
                            <View style = {styles.answerContainer}>
                                <Text style = {styles.resultStyle}> {es.gameScreen.correctAnswers}</Text>
                                <Text style = {styles.answerNumberStyle}>{correctAnswers}</Text>
                            </View>
                            
                                
                            <View style = {styles.answerContainer}>
                            <Text style = {styles.resultStyle}>{es.gameScreen.wrongAnswers}</Text>
                                <Text style = {styles.answerNumberStyle}>{TOTAL_QUESTIONS - correctAnswers}</Text>

                            </View>
                                
                            </View>
                            
                        </Card>
                        
                        <View style = {styles.restartButton}>
                            <TouchableOpacity onPress = {() => this.cleanData()}>
                                <Text style = {styles.restartTextStyle}>{es.gameScreen.restartButton}</Text>
                            </TouchableOpacity>
                        </View>
                        
            
                    </View>
                   
                )
                
            }
            
        }
       return null
    }
 
}
const styles = StyleSheet.create({
    container: {
        flex:1, 
        backgroundColor: 'white'
    },
    questionStyle:{
        fontSize: 18,
        color: 'white',
        alignSelf: 'center',
        textAlign: 'center', 
        marginTop: 10, 
        marginBottom: 20, 
        fontWeight: 'bold',
        marginHorizontal: 50,
        lineHeight: 24     
       
    },
    restartButton:{
        backgroundColor: '#5B2C6F',
        borderRadius: 30,
        borderWidth: 1,
        width: '90%',
        alignSelf: 'center',
        height: 50,
        alignContent: 'center',
        borderColor: '#5B2C6F',
        marginVertical: 40,
        justifyContent: 'center'
    },
    bottomStyle: {
        borderRadius: 30,
        borderWidth: 1,
        width: '90%',
        alignSelf: 'center',
        height: 50,
        alignContent: 'center',
        backgroundColor: 'white',
        borderColor: '#424949',
        marginVertical: 30,
        justifyContent: 'center'    
    },
    answerStyle:{
        alignSelf: 'center', 
        color: '#424949', 
        fontSize: 14,
        marginVertical: 5,
        fontWeight: 'bold'

    },
    scoreContainer: {
        flex: 1,
        backgroundColor: '#8E5CC3',
        justifyContent: 'space-around',
        alignContent: 'center'
        
    },
    scoreTextStyle: {
        alignSelf: 'center', 
        marginBottom: 50, 
        fontSize: 20, 
        color: '#424949'
    },
    resultStyle: {
        lineHeight: 50, 
        fontWeight: 'bold', 
        color: '#424949'
    },
    answerNumberStyle: {
        fontSize: 30, 
        color: '#424949'
    },
    answerContainer: {
        flexDirection: 'column', 
        flex: 1, 
        alignItems: 'center'
    },
    restartTextStyle: {
        color: 'white',
        fontSize: 20,
        fontWeight: 'bold',
        alignSelf: 'center'
    },
    finishTextStyle: {
        fontSize: 30, 
        color: 'white', 
        alignSelf: 'center'
    },
    questionContainerStyle: {
        backgroundColor: '#8E5CC3', 
        height: '35%', 
        justifyContent: 'center'
    }

})

export default GameScreen
