import * as React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import Home from '../screens/Home';
import QuestionsList from '../screens/QuestionsList';
import GameScreen from '../screens/GameScreen';

const Stack = createStackNavigator();

const NavigationStack =()=> {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen
          name="Home"
          component={Home}
          options={{ title: 'Welcome' }}
        />
        <Stack.Screen
            name= "QuestionList"
            component={QuestionsList}
            options={{ title: 'Question List', headerBackTitle: " " }}
        />
        <Stack.Screen
            name= "GameScreen"
            component={GameScreen}
            options={{ title: 'Play', headerBackTitle: " " }}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default NavigationStack;