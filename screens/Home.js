import React, { Component } from 'react'
import { View, Text, StyleSheet, TouchableOpacity, Image } from 'react-native'
import LargeCard from '../components/LargeCard';
import es from '../local/es';


class Home extends Component {

    constructor(props){
        super(props);
        this.state={
            url: 'https://5e16456b22b5c600140cf9bf.mockapi.io/api/v1/test',
            questions: [],
           
        }
        
    }
    componentDidMount(){
        this.getQuestion()
    }

    getQuestion(){
        fetch(this.state.url)
        .then(res => res.json())
        .then(res => { 
            this.setState({
            questions : res
        
        })})
    }

    goTo(screen){
        const {questions} = this.state
        if(questions.length === 0){
            return null
        }else{
            this.props.navigation.navigate(screen, {questions})
        }
    }
   
    render(){
        
        const {questions} = this.state
        return (
            <View style = {styles.container}>
                
                <TouchableOpacity onPress = {()=> this.goTo('QuestionList')}>
                    <View style = {styles.bottomStyle}>
                        <Text style = {[styles.textStyle, questions.length === 0 ? styles.disabledStyle : null]}>{es.home.firstButton}</Text>
                    </View>
                    
                </TouchableOpacity>
                <View style = {styles.bottomStyle}>
                    <TouchableOpacity onPress = {()=> this.goTo('GameScreen')}>
                        <Text style = {[styles.textStyle, questions.length === 0 ? styles.disabledStyle : null]}>{es.home.secondButton}</Text>
                    </TouchableOpacity>
                </View>

                <Image source= {require("../assets/images/nerd_image.png")}
                    style= {styles.imageStyle}
                />
                
            </View>
        )
    }
    
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignContent: 'center',
        backgroundColor: '#8E5CC3',
        
       
    },
    bottomStyle: {
        borderRadius: 30,
        borderWidth: 1,
        width: '50%',
        alignSelf: 'center',
        margin: 5,
        height: 50,
        backgroundColor: '#5B2C6F',
        borderColor: '#5B2C6F',
        justifyContent: 'center',
        alignItems: 'center',
        marginVertical: 20,
        

    },
    textStyle: {
        fontSize: 20,
        color: 'white'
    },
    imageStyle: {
        resizeMode: 'contain', 
        alignSelf: 'center', 
        height: 200
    },
    disabledStyle: {
        color: 'grey'
    }
})

export default Home
