import React from 'react'
import { View, Text, StyleSheet } from 'react-native'

const Card = ({children, containerStyle}) => {
    return (
        <View style = {[styles.container, containerStyle]}>
            
            {children}
    
        </View>
    )
}

const styles = StyleSheet.create({
    container:{
        alignSelf: 'center',
        width: '90%',
        margin: 20,
        borderRadius: 30,
        borderWidth: 1,
        backgroundColor: 'white',
        borderColor: 'white',
        padding: 20,

        
        
    },
   
})

export default Card
