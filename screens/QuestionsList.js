import React, { Component, PureComponent } from 'react'
import { Text, View, FlatList, StyleSheet, Animated } from 'react-native'
import Card from '../components/Card';

let delayValue = 0
export class QuestionsList extends PureComponent {
    constructor(props){
        super(props);
        this.state={
            animatedValue: new Animated.Value(0)
        }
        this.renderItem = this.renderItem.bind(this)
    }
   
    componentDidMount(){
        const {animatedValue} = this.state
        Animated.spring(animatedValue, {
            toValue:1,
            tension:5,
            useNativeDriver:true
        }).start()
    }
    renderItem({item}){
        const {animatedValue} = this.state
        delayValue = delayValue + 500;
        const translateX = animatedValue.interpolate({
            inputRange: [0, 1],
            outputRange: [delayValue, 1],
        });
        return (
            <Animated.View
                style={[{transform: [{translateX}]}]}>
                <Card>
                    <Text style = {styles.questionStyle}>{item.question}</Text>

                    {item.answers.map((answer) => {
                        return(
                        <View style = {[styles.bottomStyle, answer.isCorrect?styles.isCorrect:styles.isIncCorrect]}>
                            <Text style = {[styles.answerStyle, answer.isCorrect?styles.isCorrect:styles.isIncCorrect]}>
                                {answer.answer}
                            </Text>
                        </View>)}

                    )}
            
                </Card>
            </Animated.View>
        )
    }
    render() {
        const { questions } = this.props.route.params;
        return (
            <View style={styles.container}>
                
                <FlatList
                    data = {questions}
                    renderItem={this.renderItem}
                    keyExtractor={item => item.number + item.question}
                    initialNumToRender={4}
                />
                
            </View>
        )
    }
}

const styles= StyleSheet.create({

    container: {
        flex:1,
        alignItems: 'center',
        backgroundColor: '#8E5CC3'
        
    },
    questionStyle: {
        fontSize: 12,
        alignSelf: 'center',
        textAlign: 'center', 
        color: '#424949', 
        marginTop: 10, 
        marginBottom: 20, 
        fontWeight: 'bold'
    },
    answerStyle:{
        alignSelf: 'center',  
        fontSize: 12
   
    },
    bottomStyle: {
        borderRadius: 30,
        borderWidth: 1,
        width: '90%',
        alignSelf: 'center',
        margin: 5,
        height: 30,
        justifyContent: 'center',
        alignContent: 'center'
    },
    isCorrect:{
        backgroundColor: '#5B2C6F',
        borderColor: '#5B2C6F',
        color: 'white'
       
    },
    isIncCorrect:{
        backgroundColor: '#E5E7E9',
        borderColor: '#E5E7E9',
        color: 'black'
        
    }
})

export default QuestionsList
